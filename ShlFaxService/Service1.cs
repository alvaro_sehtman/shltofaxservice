﻿using ErrorLog.Exceptions;
using SendingFax.Sending;
using System;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.ServiceProcess;
using System.Timers;

namespace ShlFaxService
{
    public partial class Service1 : ServiceBase
    {
        Timer timer = new Timer(); // name space(using System.Timers;)  
        string _path = ConfigurationManager.AppSettings.Get("LogDirectory");
        int TimeInterval = int.Parse(ConfigurationManager.AppSettings.Get("TimeIntervalInSeconds")) * 1000;
        SendFromTable _sendFromTable = new SendFromTable();
        int Loops = 0;
        int LoopsToStatus = int.Parse(ConfigurationManager.AppSettings.Get("LoopsToStatus"));
        int ErrorMessagesEachSeconds = int.Parse(ConfigurationManager.AppSettings.Get("ErrorEachSeconds"));
        DateTime _LastErrorMessage = DateTime.MinValue;
        int CountErros = 0;
        public Service1()
        {
            InitializeComponent();
        }
        protected override void OnStart(string[] args)
        {
            WriteToFile("Service is started at " + DateTime.Now);
            timer.Elapsed += new ElapsedEventHandler(OnElapsedTime);
            timer.Interval = TimeInterval; //number in milisecinds  
            timer.Enabled = true;
        }
        protected override void OnStop()
        {
            _sendFromTable.Dispose();
            WriteToFile("Service is stopped at " + DateTime.Now);
        }
        private void OnElapsedTime(object source, ElapsedEventArgs e)
        {
            try
            {
                Loops++;
                bool a = _sendFromTable.fnSendNewFax();
                if (Loops >= LoopsToStatus)
                {
                    Loops = 0;
                    bool b = _sendFromTable.CheckStatus();
                }
                CountErros = 0;
            }
            catch (Exception ex)
            {
                if (_LastErrorMessage.AddSeconds(ErrorMessagesEachSeconds) <= DateTime.Now)
                {
                    _LastErrorMessage = DateTime.Now;
                    ErrorLog.Exceptions.LogError.WriteShortLogError(ex);
                }
                _sendFromTable.isBusy = false;
                CountErros++;
                if(CountErros >= 30)
                {
                    Process.GetCurrentProcess().Kill();
                }
            }
        }
        public void WriteToFile(string Message)
        {


            if (!Directory.Exists(_path))
            {
                Directory.CreateDirectory(_path);
            }
            string filepath = _path + @"\ServiceLog.txt";
            if (!File.Exists(filepath))
            {
                // Create a file to write to.   
                using (StreamWriter sw = File.CreateText(filepath))
                {
                    sw.WriteLine(Message);
                }
            }
            else
            {
                using (StreamWriter sw = File.AppendText(filepath))
                {
                    sw.WriteLine(Message);
                }
            }
        }
    }
}
