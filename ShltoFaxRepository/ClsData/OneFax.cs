﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShlToFaxRepository.ClsData
{
    public class OneFax
    {
       
        public long FaxId { get; set; }
        public DateTime CreateDate { get; set; }
        public byte ToFax { get; set; }
        public string FaxStatus { get; set; }
        public string FaxNum { get; set; }
        public string InterfaxError { get; set; }
        public long InterfaxId { get; set; }
        public string FileName { get; set; }
        public int Custpointer { get; set; }
        public int Station { get; set; }
        public string ToName { get; set; }
        
    }
}
