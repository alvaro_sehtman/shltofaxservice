﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShlToFaxRepository
{
    
    public interface IRepository<TEntity, TKey> where TEntity : class
     {
            TEntity Get(TKey id);
            //void Save();
            bool SaveOrUpdate(Func<TEntity, bool> predicate);
            //void Delete(TEntity entity, out bool isSucceded);
            //IEnumerable<TEntity> GetAll(out bool isSucceded);
            IEnumerable<TEntity> Find(Func<TEntity, bool> predicate);
    }
  
}
