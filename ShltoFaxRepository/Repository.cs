﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.EntityFrameworkCore;
using ShlToFaxEntities.Entities;

namespace ShlToFaxRepository
{
    public class Repository<TEntity, TKey> : IDisposable, IRepository<TEntity, TKey> where TEntity : class
    {
        protected readonly McDBContext Context;
        public TEntity Entity { get; set; }
        public List<TEntity> EntityList { get; set; }

        public Repository()
        {
            Context = new McDBContext();
            Entity = (TEntity)Activator.CreateInstance(typeof(TEntity));
        }

        public TEntity Get(TKey id)
        {
            var ent = Context.Set<TEntity>().Find(id);
            if (ent != null)
            {
                Entity = ent;
            }
            return Entity;
        }


        public TEntity getEmpty()
        {
            var x = Context.Set<TEntity>().DefaultIfEmpty().FirstOrDefault();
            return x;
        }

        public void RefreshEntity()
        {
            Entity = (TEntity)Activator.CreateInstance(typeof(TEntity)); 
        }

        public void getEmptyList()
        {
            EntityList = new List<TEntity>();
        }


        public IEnumerable<TEntity> Find(Func<TEntity, bool> predicate)
        {
           // EntityList = Context.Set<TEntity>().Where(predicate).ToList();
            return Context.Set<TEntity>().Where(predicate);
        }

        public void Dispose()
        {
            Context.Dispose();
        }

        public bool SaveOrUpdate(Func<TEntity, bool> predicate)
        {
                var ent = Context.Set<TEntity>().AsNoTracking().Where(predicate).FirstOrDefault();
                if (ent == null)
                {
                    Context.Set<TEntity>().Add(Entity);
                }
                else
                {
                    Context.Set<TEntity>().Attach(Entity);
                    Context.Set<TEntity>().Update(Entity);
                }
                Context.SaveChanges();
                return true;
        }

        public bool SimpleSave()
        {
            Context.Set<TEntity>().Add(Entity);
            Context.SaveChanges();
            return true;
        }


        public bool SaveList()
        {
            //  var ent = Context.Set<TEntity>().AsNoTracking().ToList();
            //EntityList = entityList;
          //  var ent = Context.Set<TEntity>().AsNoTracking().Where(predicate).FirstOrDefault();
            if (EntityList != null && EntityList.Count > 0)
            {
                foreach(TEntity oneElement in EntityList)
                {
                    Context.Set<TEntity>().Add(oneElement);
                }
                Context.SaveChanges();
            }
           
            return true;
        }


    }
}
