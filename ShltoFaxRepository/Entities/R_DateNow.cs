﻿
using Microsoft.EntityFrameworkCore;
using ShlToFaxEntities.Entities;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;


namespace ShltoFaxtRepository.Entities
{
    public class DateNow
    {
        [Key]
        public DateTime DateTimeNow { get; set; }
    }
    public  class R_DateNow : McDBContext
    {
        public DbSet<DateNow> DateTimeNowToShow { get; set; }

        public DateTime GetDate()
        {

            DateTime x =DateTimeNowToShow.FromSql("select getdate() as DateTimeNow ").FirstOrDefault().DateTimeNow;
            return x;
        }
        public new void Dispose()
        {
            base.Dispose();
        }
    }


    public static class GetServerDate
    {

        public static DateTime get()
        {
            using (R_DateNow myDate = new R_DateNow())
            {
                return myDate.GetDate();
            }

        }

    }





}
