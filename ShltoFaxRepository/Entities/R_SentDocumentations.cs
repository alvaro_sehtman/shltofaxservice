﻿using ShlToFaxEntities.Entities;
using ShlToFaxRepository;
using ShlToFaxRepository.ClsData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShltoFaxtRepository.Entities
{
    public class R_SentDocumentations : Repository<Sentdocumentation, Int32>, IDisposable
    {
        public Sentdocumentation GetInt(long Ptr)
        {
            var x = (from t in Context.Sentdocumentation
                     where t.Internalcounter == Ptr
                     select t).FirstOrDefault();
            if (x != null)
            {
                Entity = x;
            }
            return Entity;
        }

        public List<OneFax> GetByStatus(byte ToFax,DateTime? FromSentDate )
        {
            if (FromSentDate == null) { FromSentDate = DateTime.Now.AddDays(-1); }

            var x = (from t in Context.Sentdocumentation
                     where t.ToFax == ToFax
                     && t.Createdate >= FromSentDate
                     select new OneFax() {  FaxId = t.Internalcounter
                                           ,CreateDate = t.Createdate
                                           ,ToFax =t.ToFax
                                           ,FaxStatus =t.FaxStatus
                                           ,FaxNum=t.Sentfax
                                           ,FileName=t.FaxFilename
                                           ,InterfaxId=t.InterFaxId
                                           ,Custpointer=(int)t.Custpointer
                                           ,Station=t.Station
                                           ,ToName=t.Winuser
                                          }).ToList();
            return x;
        }

        public new void Dispose()
        {
            base.Dispose();
        }
    }
}
