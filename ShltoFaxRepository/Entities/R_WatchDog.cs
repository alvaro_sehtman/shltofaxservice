﻿using ShlToFaxEntities.Entities;
using ShlToFaxRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShltoFaxtRepository.Entities
{
    public class R_WatchDog : Repository<Watchdog, Int32>, IDisposable
    {
        public Watchdog GetByName(string AppName)
        {
            var x = (from t in Context.Watchdog
                     where t.ApplicationName == AppName
                     select t).FirstOrDefault();
            if (x != null)
            {
                Entity = x;
            }
            return Entity;
        }


    }
}
