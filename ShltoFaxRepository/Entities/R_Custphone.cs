﻿using ShlToFaxEntities.Entities;
using ShlToFaxRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShltoFaxtRepository.Entities
{
    public class R_Custphone : Repository<Custphone, int>, IDisposable
    {
        public Tuple<string,string> MainPhone(int Custpointer)
        {
            var x = (from t in Context.Custphone
                     where t.Custpointer == Custpointer
                     && t.Phonetype != 9
                     orderby t.Custmainphone descending, t.Phonetype ascending
                     select new Tuple<string,string>("0" + t.Custprefix.ToString().Trim(),
                                                     t.Custphone1.ToString().Trim())
                     ).FirstOrDefault();
            return x;
        }
        public new void Dispose()
        {
            base.Dispose();
        }
    }
}
