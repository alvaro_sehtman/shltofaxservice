﻿using ShlToFaxEntities.Entities;
using ShlToFaxRepository;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AmbulanceReportRepository.Entities
{
    public class R_Reminder: Repository<Reminder, int>, IDisposable
    {
        public List<long> GetForAmbulance(int Custpointer)
        {
            var x = Context.Reminder.
                    Where(c => c.Custpointer == Custpointer &&
                          c.Remstatus == 0 && c.Remtype == 15)
                    .Select(c => c.Internalcounter).ToList();
            return x;
        }
        public Reminder GetID(int Internalcounter)
        {
            var x = Context.Reminder.
                    Where(c => c.Internalcounter == Internalcounter)
                    .FirstOrDefault();
            if (x != null)
            {
                Entity = x;
            }
            return Entity;
        }
        public new void Dispose()
        {
            base.Dispose();
        }
    }
}
