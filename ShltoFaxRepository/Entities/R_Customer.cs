﻿using ShlToFaxRepository;
using System;
using System.Linq;
using ShlToFaxEntities.Entities;

namespace ShlToFaxtRepository.Entities
{
    public class R_Customer : Repository<Customer, Int32>, IDisposable
    {
        public Customer GetFromPTR(int Custpointer)
        {

            var x = Context.Customer.
                    Where(c => c.Custpointer == Custpointer )
                    .FirstOrDefault();

            if (x != null)
            {
                Entity = x;
            }
            return Entity;
        }

        public new void Dispose()
        {
            base.Dispose();
        }

        public int GetCustPointer(long custId)
        {
            int pointer = (from e in Context.Customer
                           where e.Custid == custId
                           select e.Custpointer).FirstOrDefault();

            return pointer;
        }


    }
}