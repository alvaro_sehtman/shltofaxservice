﻿using ShlToFaxEntities.Entities;
using ShlToFaxRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShltoFaxtRepository.Entities
{
    public class R_T141Interfaxstatus : Repository<T141interfaxstatus, int>, IDisposable
    {
        public List<Tuple<int,string,short>> GetAll()
        {

            var x = (from a in Context.T141interfaxstatus
                     select new Tuple<int, string,short> (a.Code,a.Descript,a.Failed )
                     ).ToList() ;

            
            return x;
        }

        public new void Dispose()
        {
            base.Dispose();
        }

    }
}
