﻿using System;
using System.Collections.Generic;

namespace ShlToFaxEntities.Entities
{
    public partial class T141interfaxstatus
    {
        public int Code { get; set; }
        public string Descript { get; set; }
        public string Short { get; set; }
        public short Failed { get; set; }
    }
}
