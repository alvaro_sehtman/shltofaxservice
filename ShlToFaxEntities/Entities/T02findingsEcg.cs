﻿using System;
using System.Collections.Generic;

namespace AmbulanceReportEntities.Entities
{
    public partial class T02findingsEcg
    {
        public short Id { get; set; }
        public string Name { get; set; }
        public string ShortName { get; set; }
        public string Descript { get; set; }
        public string Type { get; set; }
        public short Status { get; set; }
        public DateTime CreateDate { get; set; }
    }
}
