﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace ShlToFaxEntities.Entities
{
    public partial class McDBContext : DbContext
    {
        public McDBContext()
        {
        }

        public McDBContext(DbContextOptions<McDBContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Customer> Customer { get; set; }
        public virtual DbSet<Custphone> Custphone { get; set; }
        public virtual DbSet<Reminder> Reminder { get; set; }
        public virtual DbSet<Sentdocumentation> Sentdocumentation { get; set; }
        public virtual DbSet<T141interfaxstatus> T141interfaxstatus { get; set; }
        public virtual DbSet<T93specialusers> T93specialusers { get; set; }
        public virtual DbSet<Watchdog> Watchdog { get; set; }

//        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
//        {
//            if (!optionsBuilder.IsConfigured)
//            {
//#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
//                optionsBuilder.UseSqlServer("Server=192.168.54.33;Database=McDB;Trusted_Connection=True;");
//            }
//        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.3-servicing-35854");

            modelBuilder.Entity<Customer>(entity =>
            {
                entity.HasKey(e => e.Custpointer)
                    .HasName("PK__CUSTOMER__5086CE36");

                entity.ToTable("CUSTOMER");

                entity.HasIndex(e => e.Custid)
                    .HasName("CUSTID")
                    .IsUnique();

                entity.HasIndex(e => new { e.Homepointer, e.Custpointer })
                    .HasName("IX_HOMEPOINTER");

                entity.HasIndex(e => new { e.Lastname, e.Firstname })
                    .HasName("XCUSTOMER_NAME");

                entity.Property(e => e.Custpointer).HasColumnName("CUSTPOINTER");

                entity.Property(e => e.Alone)
                    .HasColumnName("ALONE")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.Bikurdate)
                    .HasColumnName("BIKURDATE")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(null)");

                entity.Property(e => e.Bikurrofe)
                    .HasColumnName("BIKURROFE")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.Bikurstat)
                    .HasColumnName("BIKURSTAT")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.Custid)
                    .HasColumnName("CUSTID")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.Custstatus)
                    .HasColumnName("CUSTSTATUS")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.Deceased)
                    .HasColumnName("DECEASED")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.Firstdate)
                    .HasColumnName("FIRSTDATE")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Firstname)
                    .IsRequired()
                    .HasColumnName("FIRSTNAME")
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Homepointer).HasColumnName("HOMEPOINTER");

                entity.Property(e => e.Homepointer2)
                    .HasColumnName("HOMEPOINTER2")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.Insurance)
                    .IsRequired()
                    .HasColumnName("INSURANCE")
                    .HasMaxLength(16)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Insurancecode)
                    .HasColumnName("INSURANCECODE")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.Internet)
                    .HasColumnName("INTERNET")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.Langcode)
                    .HasColumnName("LANGCODE")
                    .HasDefaultValueSql("(1)");

                entity.Property(e => e.Lastname)
                    .IsRequired()
                    .HasColumnName("LASTNAME")
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Localcustid)
                    .IsRequired()
                    .HasColumnName("LOCALCUSTID")
                    .HasMaxLength(16)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Localparentid)
                    .IsRequired()
                    .HasColumnName("LOCALPARENTID")
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Mainservice)
                    .HasColumnName("MAINSERVICE")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.Micucode)
                    .HasColumnName("MICUCODE")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.Middlename)
                    .IsRequired()
                    .HasColumnName("MIDDLENAME")
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.MsreplTranVersion)
                    .HasColumnName("msrepl_tran_version")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.Nameprefix)
                    .IsRequired()
                    .HasColumnName("NAMEPREFIX")
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Parentid)
                    .HasColumnName("PARENTID")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.Parentpointer)
                    .HasColumnName("PARENTPOINTER")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.Paydate)
                    .HasColumnName("PAYDATE")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Payment)
                    .HasColumnName("PAYMENT")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.Paystat)
                    .HasColumnName("PAYSTAT")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.Pivotal)
                    .HasColumnName("PIVOTAL")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.Program)
                    .HasColumnName("PROGRAM")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.Receipt)
                    .HasColumnName("RECEIPT")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.Roomnumber)
                    .IsRequired()
                    .HasColumnName("ROOMNUMBER")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Serv1agent)
                    .HasColumnName("SERV1AGENT")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.Serv1brnch)
                    .HasColumnName("SERV1BRNCH")
                    .HasDefaultValueSql("(1)");

                entity.Property(e => e.Serv1code)
                    .HasColumnName("SERV1CODE")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.Serv1date)
                    .HasColumnName("SERV1DATE")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Serv1form)
                    .HasColumnName("SERV1FORM")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.Serv1priod)
                    .HasColumnName("SERV1PRIOD")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.Serv1priodtype)
                    .HasColumnName("SERV1PRIODTYPE")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.Serv1type)
                    .HasColumnName("SERV1TYPE")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.Sex)
                    .HasColumnName("SEX")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.Specialstatus)
                    .HasColumnName("SPECIALSTATUS")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.Staffcode)
                    .HasColumnName("STAFFCODE")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.Statdate)
                    .HasColumnName("STATDATE")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");
            });

            modelBuilder.Entity<Custphone>(entity =>
            {
                entity.HasKey(e => new { e.Custpointer, e.Internalcounter })
                    .HasName("PK__CUSTPHONE__591C1437");

                entity.ToTable("CUSTPHONE");

                entity.HasIndex(e => new { e.Custpointer, e.Phonetype })
                    .HasName("IX_CUSTPHONE");

                entity.HasIndex(e => new { e.Custprefix, e.Custphone1 })
                    .HasName("XCUSTPHONE_PHONE");

                entity.Property(e => e.Custpointer).HasColumnName("CUSTPOINTER");

                entity.Property(e => e.Internalcounter)
                    .HasColumnName("INTERNALCOUNTER")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Custcountryprefix)
                    .HasColumnName("CUSTCOUNTRYPREFIX")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.Custmainphone)
                    .HasColumnName("CUSTMAINPHONE")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.Custphone1)
                    .HasColumnName("CUSTPHONE")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.Custprefix)
                    .HasColumnName("CUSTPREFIX")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.Exchangeprefix)
                    .HasColumnName("EXCHANGEPREFIX")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.MsreplTranVersion)
                    .HasColumnName("msrepl_tran_version")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.Phoneproperties)
                    .HasColumnName("PHONEPROPERTIES")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.Phonetype)
                    .HasColumnName("PHONETYPE")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.Wholephone)
                    .HasColumnName("WHOLEPHONE")
                    .HasComputedColumnSql("(convert(bigint,(ltrim(rtrim(convert(char,[CUSTPREFIX]))) + ltrim(rtrim(convert(char,[CUSTPHONE]))))))");
            });

            modelBuilder.Entity<Reminder>(entity =>
            {
                entity.HasKey(e => new { e.Fromuser, e.Fromdatetime, e.Internalcounter })
                    .HasName("PK__REMINDER__1BF3D5BD")
                    .ForSqlServerIsClustered(false);

                entity.ToTable("REMINDER");

                entity.HasIndex(e => e.Dodatetime)
                    .HasName("IX_REMINDER_2")
                    .ForSqlServerIsClustered();

                entity.HasIndex(e => e.Internalcounter)
                    .HasName("IX_REMINDER");

                entity.HasIndex(e => new { e.Custpointer, e.Remstatus })
                    .HasName("IX_CUSTPOINTER");

                entity.HasIndex(e => new { e.Remstatus, e.Touser })
                    .HasName("IX_REMINDER_1");

                entity.Property(e => e.Fromuser)
                    .HasColumnName("FROMUSER")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.Fromdatetime)
                    .HasColumnName("FROMDATETIME")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Internalcounter)
                    .HasColumnName("INTERNALCOUNTER")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Custaddress)
                    .IsRequired()
                    .HasColumnName("CUSTADDRESS")
                    .HasMaxLength(250)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Custname)
                    .IsRequired()
                    .HasColumnName("CUSTNAME")
                    .HasMaxLength(26)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Custphone)
                    .HasColumnName("CUSTPHONE")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.Custpointer)
                    .HasColumnName("CUSTPOINTER")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.Custprefix)
                    .HasColumnName("CUSTPREFIX")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.Dodatetime)
                    .HasColumnName("DODATETIME")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Dostatus)
                    .HasColumnName("DOSTATUS")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.Dotime)
                    .HasColumnName("DOTIME")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.Fromname)
                    .IsRequired()
                    .HasColumnName("FROMNAME")
                    .HasMaxLength(15)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Fromtime)
                    .HasColumnName("FROMTIME")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.Mainservice)
                    .HasColumnName("MAINSERVICE")
                    .HasDefaultValueSql("(1)");

                entity.Property(e => e.MsreplTranVersion)
                    .HasColumnName("msrepl_tran_version")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.Remark)
                    .IsRequired()
                    .HasColumnName("REMARK")
                    .HasMaxLength(250)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Remstatus)
                    .HasColumnName("REMSTATUS")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.Remtype)
                    .HasColumnName("REMTYPE")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.Subject)
                    .IsRequired()
                    .HasColumnName("SUBJECT")
                    .HasMaxLength(80)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Toname)
                    .IsRequired()
                    .HasColumnName("TONAME")
                    .HasMaxLength(25)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Touser)
                    .HasColumnName("TOUSER")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.Userdone)
                    .HasColumnName("USERDONE")
                    .HasDefaultValueSql("(0)");
            });

            modelBuilder.Entity<Sentdocumentation>(entity =>
            {
                entity.HasKey(e => e.Internalcounter)
                    .HasName("PK_SENTDOCUMANTATION")
                    .ForSqlServerIsClustered(false);

                entity.ToTable("SENTDOCUMENTATION");

                entity.HasIndex(e => e.Createdate)
                    .HasName("IX_SENTDOCUMANTATION");

                entity.HasIndex(e => e.Custpointer)
                    .HasName("IX_SENTDOCUMANTATION_1")
                    .ForSqlServerIsClustered();

                entity.Property(e => e.Internalcounter).HasColumnName("INTERNALCOUNTER");

                entity.Property(e => e.Calldate)
                    .HasColumnName("CALLDATE")
                    .HasColumnType("datetime");

                entity.Property(e => e.Controlpointer).HasColumnName("CONTROLPOINTER");

                entity.Property(e => e.Createdate)
                    .HasColumnName("CREATEDATE")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Custpointer).HasColumnName("CUSTPOINTER");

                entity.Property(e => e.EcgFilename)
                    .IsRequired()
                    .HasColumnName("ECG_FILENAME")
                    .HasMaxLength(500)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.EcgOrigFilename)
                    .IsRequired()
                    .HasColumnName("ECG_ORIG_FILENAME")
                    .HasMaxLength(500)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Ecgdate)
                    .HasColumnName("ECGDATE")
                    .HasColumnType("datetime");

                entity.Property(e => e.FaxDatetimeSent)
                    .HasColumnName("FAX_DATETIME_SENT")
                    .HasColumnType("datetime");

                entity.Property(e => e.FaxFilename)
                    .HasColumnName("FAX_FILENAME")
                    .HasMaxLength(500);

                entity.Property(e => e.FaxStatus)
                    .IsRequired()
                    .HasColumnName("FAX_STATUS")
                    .HasMaxLength(50)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.FaxTries).HasColumnName("FAX_TRIES");

                entity.Property(e => e.Filepassword)
                    .IsRequired()
                    .HasColumnName("FILEPASSWORD")
                    .HasMaxLength(50)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Hospital)
                    .IsRequired()
                    .HasColumnName("HOSPITAL")
                    .HasMaxLength(500)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.InterFaxError)
                    .IsRequired()
                    .HasColumnName("INTER_FAX_ERROR")
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.InterFaxId).HasColumnName("INTER_FAX_ID");

                entity.Property(e => e.PhonePassword)
                    .IsRequired()
                    .HasColumnName("PHONE_PASSWORD")
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.PrefixPassword)
                    .IsRequired()
                    .HasColumnName("PREFIX_PASSWORD")
                    .HasMaxLength(50)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.PrescriptFilename)
                    .IsRequired()
                    .HasColumnName("PRESCRIPT_FILENAME")
                    .HasMaxLength(500)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Printed)
                    .IsRequired()
                    .HasColumnName("PRINTED")
                    .HasMaxLength(50)
                    .HasDefaultValueSql("('לא')");

                entity.Property(e => e.Sentemail)
                    .IsRequired()
                    .HasColumnName("SENTEMAIL")
                    .HasMaxLength(500)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Sentfax)
                    .IsRequired()
                    .HasColumnName("SENTFAX")
                    .HasMaxLength(500)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Station)
                    .HasColumnName("STATION")
                    .HasDefaultValueSql("((999))");

                entity.Property(e => e.ToFax).HasColumnName("TO_FAX");

                entity.Property(e => e.Winuser)
                    .IsRequired()
                    .HasColumnName("WINUSER")
                    .HasMaxLength(500)
                    .HasDefaultValueSql("(suser_sname())");
            });

            modelBuilder.Entity<T141interfaxstatus>(entity =>
            {
                entity.HasKey(e => e.Code)
                    .ForSqlServerIsClustered(false);

                entity.ToTable("T141INTERFAXSTATUS");

                entity.HasIndex(e => e.Code)
                    .HasName("IX_T141INTERFAXSTATUS")
                    .IsUnique()
                    .ForSqlServerIsClustered();

                entity.Property(e => e.Code)
                    .HasColumnName("CODE")
                    .ValueGeneratedNever();

                entity.Property(e => e.Descript)
                    .IsRequired()
                    .HasColumnName("DESCRIPT")
                    .HasMaxLength(255);

                entity.Property(e => e.Failed).HasColumnName("FAILED");

                entity.Property(e => e.Short)
                    .IsRequired()
                    .HasColumnName("SHORT")
                    .HasMaxLength(50)
                    .HasDefaultValueSql("('')");
            });

            modelBuilder.Entity<T93specialusers>(entity =>
            {
                entity.HasKey(e => e.Code)
                    .HasName("PK_T92SPECIALUSERS");

                entity.ToTable("T93SPECIALUSERS");

                entity.Property(e => e.Code)
                    .HasColumnName("CODE")
                    .ValueGeneratedNever();

                entity.Property(e => e.Active)
                    .HasColumnName("ACTIVE")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.Company).HasColumnName("COMPANY");

                entity.Property(e => e.Descript)
                    .IsRequired()
                    .HasColumnName("DESCRIPT")
                    .HasMaxLength(60)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Display).HasColumnName("DISPLAY");

                entity.Property(e => e.Remarks)
                    .HasColumnName("REMARKS")
                    .HasMaxLength(250);

                entity.Property(e => e.Short)
                    .IsRequired()
                    .HasColumnName("SHORT")
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");
            });

            modelBuilder.Entity<Watchdog>(entity =>
            {
                entity.HasKey(e => e.Internalcounter);

                entity.ToTable("WATCHDOG");

                entity.Property(e => e.Internalcounter).HasColumnName("INTERNALCOUNTER");

                entity.Property(e => e.ApplicationName)
                    .HasColumnName("APPLICATION_NAME")
                    .HasMaxLength(50);

                entity.Property(e => e.Createdate)
                    .HasColumnName("CREATEDATE")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Emailsent).HasColumnName("EMAILSENT");

                entity.Property(e => e.Lastemail)
                    .HasColumnName("LASTEMAIL")
                    .HasColumnType("datetime");

                entity.Property(e => e.Lastrundate)
                    .HasColumnName("LASTRUNDATE")
                    .HasColumnType("datetime");

                entity.Property(e => e.Lastsms)
                    .HasColumnName("LASTSMS")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Lastsucceddate)
                    .HasColumnName("LASTSUCCEDDATE")
                    .HasColumnType("datetime");

                entity.Property(e => e.Programstatus).HasColumnName("PROGRAMSTATUS");

                entity.Property(e => e.Programstatusdate)
                    .HasColumnName("PROGRAMSTATUSDATE")
                    .HasColumnType("datetime");

                entity.Property(e => e.Smssent).HasColumnName("SMSSENT");
            });
        }
    }
}
