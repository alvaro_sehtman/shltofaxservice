﻿using System;
using System.Collections.Generic;

namespace ShlToFaxEntities.Entities
{
    public partial class Sentdocumentation
    {
        public long Internalcounter { get; set; }
        public long Custpointer { get; set; }
        public string PrescriptFilename { get; set; }
        public string EcgFilename { get; set; }
        public string Winuser { get; set; }
        public DateTime Createdate { get; set; }
        public DateTime? Calldate { get; set; }
        public string Sentemail { get; set; }
        public long? Controlpointer { get; set; }
        public DateTime? Ecgdate { get; set; }
        public string Sentfax { get; set; }
        public string EcgOrigFilename { get; set; }
        public string Hospital { get; set; }
        public string Filepassword { get; set; }
        public string PrefixPassword { get; set; }
        public string PhonePassword { get; set; }
        public string Printed { get; set; }
        public byte ToFax { get; set; }
        public string FaxStatus { get; set; }
        public DateTime? FaxDatetimeSent { get; set; }
        public short FaxTries { get; set; }
        public string InterFaxError { get; set; }
        public long InterFaxId { get; set; }
        public string FaxFilename { get; set; }
        public int Station { get; set; }
    }
}
