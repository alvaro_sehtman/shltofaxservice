﻿using System;
using System.Collections.Generic;

namespace ShlToFaxEntities.Entities
{
    public partial class Customer
    {
        public int Custpointer { get; set; }
        public long Custid { get; set; }
        public string Localcustid { get; set; }
        public string Nameprefix { get; set; }
        public string Lastname { get; set; }
        public string Middlename { get; set; }
        public string Firstname { get; set; }
        public byte Sex { get; set; }
        public int Homepointer { get; set; }
        public double Payment { get; set; }
        public DateTime Paydate { get; set; }
        public int Receipt { get; set; }
        public short Serv1code { get; set; }
        public int Serv1agent { get; set; }
        public DateTime Serv1date { get; set; }
        public short Serv1priod { get; set; }
        public byte Serv1priodtype { get; set; }
        public byte Serv1type { get; set; }
        public byte Serv1form { get; set; }
        public byte Custstatus { get; set; }
        public short Serv1brnch { get; set; }
        public long Parentid { get; set; }
        public string Localparentid { get; set; }
        public int Parentpointer { get; set; }
        public DateTime Firstdate { get; set; }
        public byte Paystat { get; set; }
        public byte Alone { get; set; }
        public short Program { get; set; }
        public byte Deceased { get; set; }
        public DateTime Statdate { get; set; }
        public int Staffcode { get; set; }
        public byte Internet { get; set; }
        public short Langcode { get; set; }
        public string Insurance { get; set; }
        public byte Specialstatus { get; set; }
        public string Roomnumber { get; set; }
        public byte Mainservice { get; set; }
        public byte Bikurrofe { get; set; }
        public byte Bikurstat { get; set; }
        public DateTime? Bikurdate { get; set; }
        public byte Pivotal { get; set; }
        public int Insurancecode { get; set; }
        public short Micucode { get; set; }
        public Guid MsreplTranVersion { get; set; }
        public int Homepointer2 { get; set; }
    }
}
