﻿using System;
using System.Collections.Generic;

namespace AmbulanceReportEntities.Entities
{
    public partial class AmbulanceShift
    {
        public int Id { get; set; }
        public DateTime StartDateTime { get; set; }
        public DateTime? EndDateTime { get; set; }
        public int AmbulanceId { get; set; }
        public string Phone { get; set; }
        public int DoctorId { get; set; }
        public int ParamedicId { get; set; }
        public int DriverId { get; set; }
        public int ConnectionStatus { get; set; }
        public int TeamStatus { get; set; }
        public DateTime? CreateDate { get; set; }
        public int UserId { get; set; }
    }
}
