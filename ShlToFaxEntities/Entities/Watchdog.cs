﻿using System;
using System.Collections.Generic;

namespace ShlToFaxEntities.Entities
{
    public partial class Watchdog
    {
        public short Internalcounter { get; set; }
        public DateTime Createdate { get; set; }
        public DateTime? Lastrundate { get; set; }
        public DateTime? Lastsucceddate { get; set; }
        public byte? Programstatus { get; set; }
        public byte? Smssent { get; set; }
        public byte? Emailsent { get; set; }
        public DateTime? Programstatusdate { get; set; }
        public DateTime Lastemail { get; set; }
        public DateTime? Lastsms { get; set; }
        public string ApplicationName { get; set; }
    }
}
