﻿using System;
using System.Collections.Generic;

namespace ShlToFaxEntities.Entities
{
    public partial class T93specialusers
    {
        public int Code { get; set; }
        public string Short { get; set; }
        public string Descript { get; set; }
        public short Active { get; set; }
        public short Company { get; set; }
        public string Remarks { get; set; }
        public byte Display { get; set; }
    }
}
