﻿using System;
using System.Collections.Generic;

namespace ShlToFaxEntities.Entities
{
    public partial class Custphone
    {
        public int Custpointer { get; set; }
        public int Internalcounter { get; set; }
        public byte Custmainphone { get; set; }
        public byte Phonetype { get; set; }
        public byte Phoneproperties { get; set; }
        public byte? Exchangeprefix { get; set; }
        public short Custcountryprefix { get; set; }
        public int Custprefix { get; set; }
        public int Custphone1 { get; set; }
        public Guid MsreplTranVersion { get; set; }
        public long? Wholephone { get; set; }
    }
}
