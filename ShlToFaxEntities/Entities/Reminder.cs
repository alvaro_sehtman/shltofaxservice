﻿using System;
using System.Collections.Generic;

namespace ShlToFaxEntities.Entities
{
    public partial class Reminder
    {
        public int Fromuser { get; set; }
        public DateTime Fromdatetime { get; set; }
        public string Fromname { get; set; }
        public short Fromtime { get; set; }
        public int Custpointer { get; set; }
        public string Custname { get; set; }
        public int Custprefix { get; set; }
        public int Custphone { get; set; }
        public string Custaddress { get; set; }
        public string Subject { get; set; }
        public string Remark { get; set; }
        public int Touser { get; set; }
        public string Toname { get; set; }
        public DateTime Dodatetime { get; set; }
        public short Dotime { get; set; }
        public byte Remtype { get; set; }
        public byte Remstatus { get; set; }
        public byte Dostatus { get; set; }
        public int Userdone { get; set; }
        public Guid MsreplTranVersion { get; set; }
        public byte Mainservice { get; set; }
        public long Internalcounter { get; set; }
    }
}
