﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using InterFAX.Api.Dtos;
using SendingFax.Globals;
using ShlToFaxRepository.ClsData;
using ShltoFaxtRepository.Entities;

namespace SendingFax.Sending
{
    public  class SendFromTable
    {
        WatchDog DbWatchDog = new WatchDog("InterFaxApp");
        public SuccessUpdate _updateStatus = new SuccessUpdate();
        public SendFromTable()
        {
            Globals.Globals.getKeys();
            DbWatchDog.UpdateActive();
        }
        public  bool isBusy = false;
        public  bool fnSendNewFax()
        {
            if (!isBusy)
            {
                DbWatchDog.UpdateRunning();
                isBusy = true;
                List<OneFax> ListFaxToSend = new FaxesInTable().Get(1); // 1== status to send
                foreach(OneFax Fax in ListFaxToSend)
                {
                    if(Fax.CreateDate.AddMinutes(Globals.Globals.TimeToFail) < GetServerDate.get()) { new FailedUpdate().Update(Fax); continue; }
                    if(Fax.FileName==null || Fax.FileName.Trim() == "") { new FailedUpdate().Update(Fax,"חסר שם קובץ"); continue; }
                    if (Globals.Globals.isTest) { Fax.FaxNum = Globals.Globals.FaxTestNumber; }
                    if (Fax.FaxNum.Split('-')[0].Length == 1) { Fax.FaxNum = "0" + Fax.FaxNum; }
                    if (Fax.FaxNum.Length < 10 || Fax.FaxNum.Split('-').Length<2 )  { new FailedUpdate().Update(Fax,"מספר לא תקין"); continue; }
                    string newPath = Globals.Globals.FaxFilePath;
                    string oldPath = @"f:\";
                    Fax.FileName= Fax.FileName.ToLower().Replace(oldPath, newPath);
                    if (!File.Exists(Fax.FileName)) { new FailedUpdate().Update(Fax, "שם הקובץ לא קיים"); continue; }
                    byte[] FaxBytes = File.ReadAllBytes(Fax.FileName);
                    long InterFaxRef=0;
                    string FaxToSend = "";
                    FaxToSend = "+972" + int.Parse(Fax.FaxNum.Split('-')[0]).ToString() + Fax.FaxNum.Split('-')[1];  
                    Task t = Task.Run(async () =>  { InterFaxRef= await new SendFax().fnSendFaxAsync(FaxToSend, FaxBytes); });
                    t.Wait();
                    Fax.InterfaxId = InterFaxRef;
                    _updateStatus.Update(Fax);
                }
                isBusy = false;
                DbWatchDog.UpdateSucced();
            }
            return true;
        }
        public bool CheckStatus()
        {
            if (!isBusy)
            {
                DbWatchDog.UpdateRunning();
                isBusy = true;
                List<OneFax> ListFaxToSend = new FaxesInTable().Get(2); // 2== sent
                foreach (OneFax Fax in ListFaxToSend)
                {
                    OutboundFaxSummary statusResult=null;
                    Task t = Task.Run(async () => { statusResult = await new SendFax().CheckStatusAsync(Fax.InterfaxId); });
                    t.Wait();
                    if (statusResult!=null)
                    {
                        _updateStatus.Status(Fax,  statusResult);
                    }
                }
                isBusy = false;
                DbWatchDog.UpdateSucced();
            }
            return true;
        }
        public void Dispose()
        {
            DbWatchDog.UpdateInactive();
            DbWatchDog = null;
        }
    }
}
