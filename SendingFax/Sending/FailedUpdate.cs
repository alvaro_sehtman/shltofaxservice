﻿using ShlToFaxRepository.ClsData;
using ShltoFaxtRepository.Entities;
using ShlToFaxtRepository.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SendingFax.Sending
{
    public class FailedUpdate
    {

      
        public void Update(OneFax Fax,string strFaxStatus= "נכשל")
        {
            using(R_SentDocumentations RSentDocumentations = new R_SentDocumentations())
            {
                var a = RSentDocumentations.GetInt(Fax.FaxId);
                RSentDocumentations.Entity.ToFax = 4; // נכשל
                RSentDocumentations.Entity.FaxStatus = strFaxStatus;
                RSentDocumentations.SaveOrUpdate(d => d.Internalcounter == Fax.FaxId);
                // to do
                fnSendReminder(Fax, "sending");

            }
        }


        public bool fnSendReminder(OneFax Fax, string pSubject)
        {
            string strReminder = "";
            if (pSubject == "sending") {  strReminder = "שליחת הפקס נכשלה בשרת הפקס בלי הודעה, נא לפנות לתמיכה .";  }
            else { strReminder = pSubject; }   
          
            Reminder toReminder= new Reminder();
            toReminder.Custpointer = Fax.Custpointer;
            toReminder.Subject = "פקס לא נשלח ליעד";
            toReminder.Remarks = strReminder;
            toReminder.ToUser = Fax.Station;
            toReminder.ToUserName = Fax.ToName;

            using (R_Customer RCustomer = new R_Customer())
            {
                RCustomer.GetFromPTR(Fax.Custpointer);
                toReminder.CustName = RCustomer.Entity.Firstname.Trim() + " " + RCustomer.Entity.Lastname.Trim();
            }

            using (R_Custphone rCustphone = new R_Custphone())
            {
                Tuple<string,string> onePhone = rCustphone.MainPhone(Fax.Custpointer);
                toReminder.Custprefix = int.Parse(onePhone.Item1);
                toReminder.Custphone = int.Parse(onePhone.Item2);
            }
            toReminder.Create();
            toReminder.ToUser = 1;
            toReminder.ToUserName = "אחראי הזנקות";
            toReminder.Create();
            return true;
        }
    }
}
//if (pSubject == "before interface")
//    strReminder = "שליחת הפקס נכשל לפני שהתקבל בשרת פקס. כנראה יש בעיה בשרת פקס בשחל.";
//else if (pSubject == "InterFax msg")
//    strReminder = "שליחת הפקס נכשלה בשרת הפקס, כנראה יש בעיה בפקס המקבל. לבדוק את מספר הפקס והאם הפקס זמין.";
//else if (pSubject == "sending")
//    strReminder = "שליחת הפקס נכשלה בשרת הפקס בלי הודעה, נא לפנות לתמיכה .";
//else if (pSubject == "After 10")
//    strReminder = " שליחת הפקס נכשלה אחרי 10 ניסיונות. " + "כנראה יש בעיה בפקס המקבל. לבדוק את מספר הפקס והאם הפקס זמין";
