﻿using InterFAX.Api.Dtos;
using ShlToFaxRepository.ClsData;
using ShltoFaxtRepository.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SendingFax.Sending
{
    public class SuccessUpdate
    {
        List<string> StatusList =new List<string>() { "","", "בתהליך", "נשלח בהצלחה", "נכשל" };
        List<int> ErrorsFailed ;
        List<Tuple<int, string, short>> allStatus;

        public void Update(OneFax Fax)
        {   FillErrors();
            using (R_SentDocumentations RSentDocumentations = new R_SentDocumentations())
            {
                var a = RSentDocumentations.GetInt(Fax.FaxId);
                RSentDocumentations.Entity.ToFax = 2; //  "בתהליך"
                RSentDocumentations.Entity.FaxStatus = "בתהליך";
                RSentDocumentations.Entity.InterFaxId = Fax.InterfaxId;
                RSentDocumentations.SaveOrUpdate(d => d.Internalcounter == Fax.FaxId);
            }
        }
        public void Status(OneFax Fax, OutboundFaxSummary statusResult)
        {
            FillErrors();
            using (R_SentDocumentations RSentDocumentations = new R_SentDocumentations())
            {
                var a = RSentDocumentations.GetInt(Fax.FaxId);
                if (statusResult.Status==0)
                {
                    RSentDocumentations.Entity.ToFax = (byte)Globals.Globals.enumToFax.sentSucceded;
                    RSentDocumentations.Entity.FaxStatus = StatusList[(byte)Globals.Globals.enumToFax.sentSucceded];
                    RSentDocumentations.Entity.FaxDatetimeSent = GetServerDate.get();
                }
               if(statusResult.Status > 0 && (ErrorsFailed.Contains(statusResult.Status)  || statusResult.AttemptsMade>=3))
                {
                    RSentDocumentations.Entity.ToFax = (byte)Globals.Globals.enumToFax.failed;
                    RSentDocumentations.Entity.FaxStatus = StatusList[(byte)Globals.Globals.enumToFax.failed];
                    
                }
                if (allStatus.Any(s=>s.Item1== statusResult.Status)) { RSentDocumentations.Entity.InterFaxError = allStatus.Find(f => f.Item1 == statusResult.Status).Item2; }
                RSentDocumentations.Entity.FaxTries = short.Parse(statusResult.AttemptsMade.ToString());
                RSentDocumentations.SaveOrUpdate(d => d.Internalcounter == Fax.FaxId);
                if (RSentDocumentations.Entity.ToFax == (byte)Globals.Globals.enumToFax.failed)
                {
                    string Subject = "";
                    if (RSentDocumentations.Entity.InterFaxError.Length > 250) { Subject = RSentDocumentations.Entity.InterFaxError.Substring(0, 250); }
                    else { Subject = RSentDocumentations.Entity.InterFaxError; }
                    new FailedUpdate().fnSendReminder(Fax, Subject);
                }
            }
        }
        void FillErrors()
        {
            if (ErrorsFailed == null)
            {
                using (R_T141Interfaxstatus RT141 = new R_T141Interfaxstatus())
                {
                    allStatus = RT141.GetAll();
                    ErrorsFailed = allStatus.Where(w => w.Item3 == 1).Select(s => s.Item1).ToList();
                }
            }
        }
    }
}
