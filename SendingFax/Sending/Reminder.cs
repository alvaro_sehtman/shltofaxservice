﻿using AmbulanceReportRepository.Entities;
using ShltoFaxtRepository.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SendingFax.Sending
{
    public class Reminder
    {
        public string Subject { get; set; }
        public string Remarks { get; set; }
        public int ToUser { get; set; }
        public int Custpointer { get; set; }
        public string ToUserName { get; set; }
        public string CustName { get; set; }
        public int Custprefix { get; set; }
        public int Custphone { get; set; }

        public bool Create()
        {
            using (R_Reminder RRem = new R_Reminder())
            {
                RRem.getEmpty();
                RRem.Entity.Fromtime = (short)( GetServerDate.get().Hour * 100);
                RRem.Entity.Fromdatetime = GetServerDate.get();
                RRem.Entity.Custpointer = Custpointer;
                RRem.Entity.Custname = CustName;
                RRem.Entity.Custprefix = Custprefix;
                RRem.Entity.Custphone = Custphone;
                RRem.Entity.Subject = Subject;
                RRem.Entity.Remark = Remarks;
                RRem.Entity.Fromname = "InterFaxApp";
                RRem.Entity.Fromuser = 999;
                RRem.Entity.Touser = ToUser;
                RRem.Entity.Toname = ToUserName;
                RRem.Entity.Dodatetime = GetServerDate.get();
                RRem.Entity.Dotime= (short)(GetServerDate.get().Hour * 100);
                RRem.Entity.Mainservice = 1;
                RRem.SimpleSave();
            }

            return true;
        }
    }
}
