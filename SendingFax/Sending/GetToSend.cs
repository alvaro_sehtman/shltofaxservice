﻿using ShlToFaxRepository.ClsData;
using ShltoFaxtRepository.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SendingFax.Sending
{
    public class FaxesInTable
    {
        public List<OneFax> Get(byte ToFax)
        {
            using (R_SentDocumentations rSent = new R_SentDocumentations())
            {
                return rSent.GetByStatus(ToFax,DateTime.Now.AddDays(-1));
            }
        }
    }
}
