﻿using InterFAX.Api;
using InterFAX.Api.Dtos;
using System.Net;
using System.Threading.Tasks;

namespace SendingFax.Sending
{
    public class SendFax
    {
        FaxClient interfax;

        public SendFax()
        {
            interfax = new FaxClient("SHL_Fax", "9r4KwZmhE3kD");
        }

        public async Task<long> fnSendFaxAsync(string FaxNum, byte[] FaxBytes)
        {
            
            SendOptions myOptions = new SendOptions();
            IFaxDocument myFaxDoc = interfax.Documents.BuildFaxDocument(FaxBytes, ".pdf");
            myOptions.FaxNumber = FaxNum;
            myOptions.PageResolution = InterFAX.Api.Dtos.PageResolution.Fine;
            myOptions.RetriesToPerform = 3;
            myOptions.ShouldScale = true;
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            long x = await interfax.Outbound.SendFax(myFaxDoc, myOptions);
            return x;
        }

        public async Task<OutboundFaxSummary> CheckStatusAsync(long InterfaxId)
        {
            //var a = await interfax.Outbound.GetFaxRecord(InterfaxId);
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            // InterFAX.Api.Dtos.OutboundFaxSummary Result = await interfax.Outbound.GetFaxRecord(InterfaxId);
            OutboundFaxSummary Result = await interfax.Outbound.GetFaxRecord(InterfaxId);
            return Result;
        }


    }
}
