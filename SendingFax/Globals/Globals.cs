﻿using Config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SendingFax.Globals
{
    public static class Globals
    {
        // public static string directoryName =Environment.CurrentDirectory;
        public static string directoryName = @"C:\ShlToFax";
        public static string ConfigPath = directoryName + @"\Configs\";
        public static string ConfigFileName = directoryName + @"\Configs\ShlToFax.Special";

        public static bool isTest;
        public static int TimeToFail;
        public static string FaxTestNumber;
        public static string FaxFilePath;

        public enum enumToFax
        {
            // 0- not relevant ; 1- to send ; 2- sending ; 3- sent succeded ;4 - failed
            notRelevant = 0,
            toSend = 1,
            sending = 2,
            sentSucceded = 3,
            failed = 4
        }

        public static void getKeys()
        {
            isTest= Config.Config.GetAppSettingSpecific(Globals.ConfigFileName, "IsTest").ToLower()=="yes" ;
            ShlToFaxEntities.ContextConnection.strConnection = Config.Config.GetAppSettingSpecific(Globals.ConfigFileName, "ConnectionString");
            TimeToFail=int.Parse(Config.Config.GetAppSettingSpecific(Globals.ConfigFileName, "TimeToFail") );
            FaxTestNumber = Config.Config.GetAppSettingSpecific(Globals.ConfigFileName, "FaxTestNumber");
            FaxFilePath = Config.Config.GetAppSettingSpecific(Globals.ConfigFileName, "FaxFilePath");
        }
      
    }
}
