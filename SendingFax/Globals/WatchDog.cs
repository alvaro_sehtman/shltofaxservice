﻿using ShltoFaxtRepository.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SendingFax.Globals
{
    public class WatchDog
    {
        public string AppName { get; set; }

        public WatchDog(string name)
        {
            AppName = name;
        }

        public void UpdateRunning()
        {
            using(R_WatchDog watch=new R_WatchDog())
            {
                var a = watch.GetByName(AppName);
                watch.Entity.Lastrundate = GetServerDate.get();
                watch.SaveOrUpdate(e => e.Internalcounter == watch.Entity.Internalcounter);
            }
        }

        public void UpdateSucced()
        {
            using (R_WatchDog watch = new R_WatchDog())
            {
                var a = watch.GetByName(AppName);
                watch.Entity.Lastsucceddate = GetServerDate.get();
                watch.SaveOrUpdate(e => e.Internalcounter == watch.Entity.Internalcounter);
            }
        }

        public void UpdateActive()
        {
            using (R_WatchDog watch = new R_WatchDog())
            {
                var a = watch.GetByName(AppName);
                watch.Entity.Programstatus = 1;
                watch.Entity.Programstatusdate= GetServerDate.get();
                watch.SaveOrUpdate(e => e.Internalcounter == watch.Entity.Internalcounter);
            }
        }

        public void UpdateInactive()
        {
            using (R_WatchDog watch = new R_WatchDog())
            {
                var a = watch.GetByName(AppName);
                watch.Entity.Programstatus = 0;
                watch.Entity.Programstatusdate = GetServerDate.get();
                watch.SaveOrUpdate(e => e.Internalcounter == watch.Entity.Internalcounter);
            }
        }
    }
}
