﻿using NLog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ErrorLog.Exceptions
{
    public static class LogError
    {
        private static Logger logger = LogManager.GetLogger("fileErrorShort");

        public static void WriteShortLogError(Exception ex)
        {
            try
            {
                var callingMethod = new StackTrace().GetFrame(1).GetMethod();
                string Method = $"{callingMethod.DeclaringType}.{callingMethod.Name}";
                string message = $"Date: {DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")} - Method:{Method} {Environment.NewLine} " +
                                 $"ErrorMsg: {ex.Message} ";
               
                logger.Error(ex, message);

            }
            catch (Exception exc)
            {
                Logger logger = LogManager.GetLogger("fileErrorShort");
                logger.Error(exc.InnerException, exc.Message);
            }
        }


    }
}
