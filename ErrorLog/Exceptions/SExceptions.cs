﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ErrorLog.Exceptions
{
    public class SExceptions : Exception
    {
        public string ErrorCode; //code to identify the error

        public SExceptions(string pErrCode, string message) : base(message)
        {
            this.Source = pErrCode;
            ErrorCode = pErrCode;
        }
        public SExceptions(string message, Exception pInnerException) : base(message, pInnerException)
        {
           
        }
    }
}
