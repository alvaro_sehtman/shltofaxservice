mkdir .\ExeService\x64 
mkdir .\ExeService\x86
mkdir .\ExeService\zh-Hans
mkdir .\ExeService\zh-Hant


copy C:\ShlToFax\ShlFaxService\bin\Debug\*.*  .\ExeService
copy C:\ShlToFax\ShlFaxService\bin\Debug\zh-Hant\*.*  .\ExeService\zh-Hant
copy C:\ShlToFax\ShlFaxService\bin\Debug\zh-Hans\*.*  .\ExeService\zh-Hans
copy C:\ShlToFax\ShlFaxService\bin\Debug\x86\*.*  .\ExeService\x86
copy C:\ShlToFax\ShlFaxService\bin\Debug\x64 \*.*  .\ExeService\x64 

DEL .\ExeService\NLog.config
DEL .\ExeService\ShlFaxService.exe.config